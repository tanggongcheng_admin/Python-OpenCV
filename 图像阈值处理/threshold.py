import cv2 as cv
import numpy as np 
import matplotlib.pyplot as plt

fileName = "lena.jpg"
img = cv.imread(fileName)

gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

"""
cv.threshold(src, x, y, method)
src -- 指原图像，原图像应该是灰度图
x -- 指用来对像素值进行分类的阈值。
y -- 指当像素值高于（有时是小于）阈值时应该被赋予的新的像素值
method -- 不同的阈值方法(cv.THRESH_BINARY, cv.THRESH_BINARY_INV, cv.THRESH_TRUNC, cv.THRESH_TOZERO, cv.THRESH_TOZERO_INV)
"""
# cv.THRESH_BINARY -- 大于阈值的像素点的灰度值设定为最大值(如8位灰度值最大为255)，灰度值小于阈值的像素点的灰度值设定为0。
ret, th_1 = cv.threshold(gray, 127, 255, cv.THRESH_BINARY)
# cv.THRESH_BINARY_INV -- 大于阈值的像素点的灰度值设定为0，而小于该阈值的设定为255。
ret, th_2 = cv.threshold(gray, 127, 255, cv.THRESH_BINARY_INV)
# cv.THRESH_TRUNC -- 像素点的灰度值小于阈值不改变，大于阈值的灰度值的像素点就设定为该阈值。
ret, th_3 = cv.threshold(gray, 127, 255, cv.THRESH_TRUNC)
# cv.THRESH_TOZERO -- 像素点的灰度值小于该阈值的不进行任何改变，而大于该阈值的部分，其灰度值全部变为0。
ret, th_4 = cv.threshold(gray, 127, 255, cv.THRESH_TOZERO)
# cv.THRESH_TOZERO_INV -- 像素点的灰度值大于该阈值的不进行任何改变，像素点的灰度值小于该阈值的，其灰度值全部变为0。
ret, th_5 = cv.threshold(gray, 127, 255, cv.THRESH_TOZERO_INV)

titles = ["Gray Image", "Binary Threshold", "Binary Threshold INV", "Trunc Threshold", "Tozero Threshold", "Tozero Threshold INV"]
images = [gray, th_1, th_2, th_3, th_4, th_5]



for i in range(len(images)):
	plt.subplot(2, 3, i + 1)
	plt.imshow(images[i], "gray")
	plt.title(titles[i])
	plt.xticks([])
	plt.yticks([])
 	
plt.show()