##################################
# cornerHarris() 有四个参数分别是：
# img - Input image, it should be grayscale and float32 type.
# blockSize - It is the size of neighbourhood considered for corner detection
# ksize - Aperture parameter of Sobel derivative used.
# k - Harris detector free parameter in the equation.
##################################

import cv2
import numpy as np

filename = 'test.jpg'
img = cv2.imread(filename)
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

gray = np.float32(gray)
dst = cv2.cornerHarris(gray,2,3,0.04)

#result is dilated for marking the corners, not important
dst = cv2.dilate(dst,None)

# Threshold for an optimal value, it may vary depending on the image.
img[dst>0.01*dst.max()]=[0,0,255]

cv2.imshow('dst',img)
if cv2.waitKey(0) & 0xff == 27:
    cv2.destroyAllWindows()



###################################
# 具有 subPixel 准确度的角落检测
# OpenCV带有一个函数cv2.cornerSubPix（），它进一步细化了以亚像素精度检测到的角点。
# 下面是一个例子。像往常一样，我们需要先找到哈里斯的角落。然后我们通过这些角的质心（可能有一些角落处的像素，我们取其质心）来优化它们。
# 哈里斯的角落标记为红色像素，精致的角落标记为绿色像素。对于这个函数，我们必须定义何时停止迭代的标准。
# 我们停止指定的迭代次数或达到一定的准确度，以先发生者为准。我们还需要定义要搜索拐角的邻域的大小。
##################################

import cv2
import numpy as np

filename = 'test2.jpg'
img = cv2.imread(filename)
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

# find Harris corners
gray = np.float32(gray)
dst = cv2.cornerHarris(gray,2,3,0.04)
dst = cv2.dilate(dst,None)
ret, dst = cv2.threshold(dst,0.01*dst.max(),255,0)
dst = np.uint8(dst)

# find centroids
ret, labels, stats, centroids = cv2.connectedComponentsWithStats(dst)

# define the criteria to stop and refine the corners
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.001)
corners = cv2.cornerSubPix(gray,np.float32(centroids),(5,5),(-1,-1),criteria)

# Now draw them
res = np.hstack((centroids,corners))
res = np.int0(res)
img[res[:,1],res[:,0]]=[0,0,255]
img[res[:,3],res[:,2]] = [0,255,0]

cv2.imshow("Image", img)
cv2.imwrite('subpixel5.png',img)

cv2.destroyAllWindows()